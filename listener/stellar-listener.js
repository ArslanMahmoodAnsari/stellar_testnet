const Last = require("../models/last-pagingToken");
const StellarSdk = require('stellar-sdk');
const server = new StellarSdk.Server('https://horizon-testnet.stellar.org');
const adminWallet = require('../models/admin-wallet');
const Transaction = require('../models/stellar-transaction');
const transactions = require('../routes/stellar-transections');


(async function stellarListener(){
  
  let aWallet = await adminWallet.findOne({});
  let {publicKey} = aWallet
  // Create an API call to query payments involving the account.
  let payments = server.payments().forAccount(publicKey);
  
  let lastToken = await loadLastPagingToken();  

  if (lastToken.pagingtoken) {
      payments.cursor(lastToken.pagingToken);
  }
  // `stream` will send each recorded payment, one by one, then keep the
  // connection open and continue to send you new payments as they occur.
  payments.stream({
      onmessage: async function(payment) {
        // Record the paging token so we can start from here next time.
        savePagingToken(payment.paging_token);
    
        // The payments stream includes both sent and received payments. We only
        // want to process received payments here.
        if (payment.to !== publicKey) {
          return;
        }
        let hash = payment.transaction_hash;
        await Transaction.findOne({ txHash: hash })
        .then(async (tran)=>{
          await transactions.deposit(tran.to,tran.value)
          .then((data)=>{
            console.log("transection successfully sent to reciever");
          }).catch((e)=>{
            console.log(e);
          })  
        })
        
    
        // In Stellar’s API, Lumens are referred to as the “native” type. Other
        // asset types have more detailed information.
        var asset;
        if (payment.asset_type === 'native') {
          asset = 'lumens';
        }
        else {
          asset = payment.asset_code + ':' + payment.asset_issuer;
        }
    
        console.log(payment.amount + ' ' + asset + ' from ' + payment.from);
      },
    
      onerror: function(error) {
        console.error('Error in payment stream',error);
      }
    });
})()

async function savePagingToken(pagingtoken) {
  await Last.findOneAndUpdate({},{pagingtoken});
}

async function loadLastPagingToken() {
  // Get the last paging token from a local database or file
  let pagingToken = await Last.findOne({})
  console.log("**********",pagingToken);
  return pagingToken;
}