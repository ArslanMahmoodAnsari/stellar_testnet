const express = require("express");
const bobyParser = require("body-parser");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 3050;
const User = require("./routes/users");
const stellarListener = require('./listener/stellar-listener');
const Transection = require('./routes/stellar-transections');
//set up bodyParser
app.use(bobyParser.json());
app.use(bobyParser.urlencoded({extended:true}));

//Routes
app.use('/user',User);
app.use('/transection',Transection);

//mongooge connection string
mongoose.promise = global.promise;
mongoose.set('useCreateIndex', true);
mongoose.connect("mongodb://root:toor12@ds137102.mlab.com:37102/stellar-testnet", { useNewUrlParser: true, useUnifiedTopology: true }).then(
	() => {
    console.log("DB connected..!!!");
	},
	err => {
	console.log("ERROR IN \"DB CONNECTIVITY\" : "  + err);
});

//running server
app.listen( PORT, () => {
	console.log(`server started on PORT => ${PORT}`);
})


