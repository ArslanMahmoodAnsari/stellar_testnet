const mongoose = require('mongoose');
const StellarSdk = require('stellar-sdk');
const fetch = require('node-fetch');


let WalletSchema =  mongoose.Schema({
    userId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"User",
        required:true
    },
    publicKey:{
        type:String,
        required:true
    },
    privateKey:{
        type:String,
        required:true
    },
    balance:{
        type:Number,
        default:10000
    },
    coin:{
        type:String,
        default:'XLM'
    }
})

// search address
WalletSchema.statics.findAnAddress = async function(publicKey){
    return await this.findOne({publicKey});
}


module.exports = mongoose.model('Wallet', WalletSchema);
