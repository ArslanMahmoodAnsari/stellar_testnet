const mongoose = require('mongoose');

let LatestTokenSchema = mongoose.Schema({
    pagingtoken:{
        type:Number,
        require:true,
        default:0
    }
})


module.exports = mongoose.model('LatestToken',LatestTokenSchema);
