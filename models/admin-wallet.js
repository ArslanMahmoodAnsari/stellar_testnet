const mongoose = require('mongoose');


let AdminWalletSchema =  mongoose.Schema({
    userId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"User",
        required:true
    },
    publicKey:{
        type:String,
        required:true
    },
    privateKey:{
        type:String,
        required:true
    },
    balance:{
        type:Number,
        default:10000
    },
    coin:{
        type:String,
        default:'XLM'
    }
})


module.exports = mongoose.model('AdminWallet', AdminWalletSchema);
