const StellarSdk = require('stellar-sdk');
const server = new StellarSdk.Server('https://horizon-testnet.stellar.org');
const router = require('express').Router();
const adminWallet = require('../models/admin-wallet');
const Wallet = require('../models/wallet');
const Transaction = require('../models/stellar-transaction');


router.post('/sendpayment', async(req,res) => {   
    let aWallet = await adminWallet.findOne({});
    let fromWallet = await Wallet.findOne({userId:req.body.sender})
    let {publicKey} = aWallet 
    let source = StellarSdk.Keypair
                .fromSecret(fromWallet.privateKey);
    let transaction;
    server.loadAccount(publicKey)
    .catch(StellarSdk.NotFoundError, (error) => {
    return res.json('The destination account does not exist!');
  })  // If there was no error, load up-to-date information on your account.
  .then(() =>{
    return server.loadAccount(source.publicKey());
  }).then((sourceAccount)=>{
    transaction = new StellarSdk.TransactionBuilder(sourceAccount,{
       fee:StellarSdk.BASE_FEE,
       networkPassphrase: StellarSdk.Networks.TESTNET 
    })
      .addOperation(StellarSdk.Operation.payment({
        destination: publicKey,
        // Because Stellar allows transaction in many currencies, you must
        // specify the asset type. The special "native" asset represents Lumens.
        asset: StellarSdk.Asset.native(),
        amount: String(req.body.transferAmmount) 
      }))      
      // A memo allows you to add your own metadata to a transaction. It's
      // optional and does not affect how Stellar treats the transaction.
      .addMemo(StellarSdk.Memo.text('Test Transaction'))
      // Wait a maximum of three minutes for the transaction
      .setTimeout(180)
      .build();
      // Sign the transaction to prove you are actually the person sending it.
      transaction.sign(source);
      // And finally, send it off to Stellar!
      return server.submitTransaction(transaction);
  })
  .then((result)=> {
    let newTransection = new Transaction({
      txHash:result.hash,
      value:req.body.transferAmmount,
      from:req.body.sender,
      to:req.body.reciever
    })
    newTransection.save().then(()=>{
      fromWallet.balance -= req.body.transferAmmount;
      fromWallet.save().then(()=>{
        console.log("ammount subtracted form sender");
        aWallet.balance += req.body.transferAmmount;
        aWallet.save().then(()=>{
          console.log("ammount added to admin");
          res.json("transection completed")
          }).catch((e)=>{
          console.log("Error",e);
          })
        }).catch((e)=>{
          console.log("Error",e);
        })
    }).catch((e)=>{
      console.log("Error",e);
    })
    
    }).catch((error) =>{
    console.error('Something went wrong!', error);
    // If the result is unknown (no response body, timeout etc.) we simply resubmit
    // already built transaction:
    // server.submitTransaction(transaction);
  });
})
async function deposit(reciever,amount){
    // let fromWallet-------------sender
    reciever = await Wallet.findOne({userId:reciever});
    let sender =  await adminWallet.findOne({});
    let {publicKey} = reciever 
    let source = StellarSdk.Keypair
                .fromSecret(sender.privateKey);
    let transaction;
    server.loadAccount(publicKey)
    .catch(StellarSdk.NotFoundError, (error) => {
    return res.json('The destination account does not exist!');
  })  // If there was no error, load up-to-date information on your account.
  .then(() =>{
    return server.loadAccount(source.publicKey());
  }).then((sourceAccount)=>{
    transaction = new StellarSdk.TransactionBuilder(sourceAccount,{
       fee:StellarSdk.BASE_FEE,
       networkPassphrase: StellarSdk.Networks.TESTNET 
    })
      .addOperation(StellarSdk.Operation.payment({
        destination: publicKey,
        // Because Stellar allows transaction in many currencies, you must
        // specify the asset type. The special "native" asset represents Lumens.
        asset: StellarSdk.Asset.native(),
        amount: String(amount) 
      }))      
      // A memo allows you to add your own metadata to a transaction. It's
      // optional and does not affect how Stellar treats the transaction.
      .addMemo(StellarSdk.Memo.text('Test Transaction'))
      // Wait a maximum of three minutes for the transaction
      .setTimeout(180)
      .build();
      // Sign the transaction to prove you are actually the person sending it.
      transaction.sign(source);
      // And finally, send it off to Stellar!
      return server.submitTransaction(transaction);
  })
  .then((result)=> {
    sender.balance -= req.body.transferAmmount;
    sender.save().then(()=>{
      console.log("ammount subtracted form admin");
      reciever.balance += req.body.transferAmmount;
      reciever.save().then(()=>{
        console.log("ammount added to reciever");
        res.json("funds transfered to user");
        }).catch((e)=>{
        console.log("Error",e);
        })
      }).catch((e)=>{
        console.log("Error",e);
      })   
    }).catch((error) =>{
    console.error('Something went wrong!', error);
    // If the result is unknown (no response body, timeout etc.) we simply resubmit
    // already built transaction:
    // server.submitTransaction(transaction);
  });
}
module.exports = router;