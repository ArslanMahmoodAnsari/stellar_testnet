const express = require("express");
const router = express.Router();
const _ = require('lodash');
const {User} = require("../models/user");//for make the user object
const Wallet = require('../models/wallet');
const StellarSdk = require('stellar-sdk');
const fetch = require('node-fetch');
let {authenticate} = require('./../middleware/authMiddleware');

createStellarKeys = async () => {
  const pair = StellarSdk.Keypair.random();
  
  try {
      const response = await fetch(
        `https://friendbot.stellar.org?addr=${encodeURIComponent(pair.publicKey())}`
      );
      const responseJSON = await response.json();
      return {publicKey:pair.publicKey(),privateKey:pair.secret()};
    } catch (e) {
      console.error("ERROR!", e);
    }
}
//Create new user - signUp/Register
router.post('/add',(req,res)=>{
    let body = _.pick(req.body, ['email', 'password']);
    let user = new User(body);
    
    user.save().then((dbUser) => {
        return user.generateAuthToken();
      }).then((token) => {
        res.header('x-auth', token);
      }).then(()=>{
        return createStellarKeys();
      }).then((keys)=>{
        let newwallet = new Wallet({
          userId:user._id,
          ...keys
        });
        return newwallet.save()
      }).then((dbWalet)=>{
        res.json('created');
      }).catch((e) => {
        console.log(e)
        res.status(400).send(e);
      })
})

// login the existing user - signIn
router.post('/login', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);
  
    User.findByCredentials(body.email, body.password).then((user) => {
      return user.generateAuthToken().then((token) => {
        res.header('x-auth', token).send(user);
      });
    }).catch((e) => {
      res.status(400).send();
    });
});

//delete a token form the user model - signOut 
router.delete('/logout', authenticate, (req, res) => {
    req.user.removeToken(req.token).then(() => {
      res.status(200).send();
    }, () => {
      res.status(400).send();
    });
});

//for checking purposes
router.get('/me',authenticate,(req,res)=>{
    res.send(req.user);
})



module.exports = router;
